package com.appetiser.module.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.appetiser.module.local.model.TrackEntity
import io.reactivex.Single

@Dao
abstract class TrackDao : BaseDao<TrackEntity> {

    @Query("SELECT * FROM TrackEntity")
    abstract fun getAllTracks(): Single<List<TrackEntity>>

    @Query("DELETE FROM TrackEntity")
    abstract fun deleteTracks()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveTracks(trackList: List<TrackEntity>)
}
