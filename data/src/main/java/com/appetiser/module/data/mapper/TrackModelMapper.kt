package com.appetiser.module.data.mapper

import com.appetiser.module.data.poko.TrackItem
import com.appetiser.module.local.model.TrackEntity
import com.appetiser.module.network.response.TrackDetailResult

fun List<TrackDetailResult>.toTrackItemList(): List<TrackItem> {
    return this.map {
        TrackItem(
            kind = it.kind,
            trackId = it.trackId,
            artistName = it.artistName,
            collectionName = it.collectionName,
            trackName = it.trackName,
            artworkUrl = it.artworkUrl100,
            collectionPrice = it.collectionPrice,
            trackPrice = it.trackPrice,
            trackRentalPrice = it.trackRentalPrice,
            collectionHdPrice = it.collectionHdPrice,
            trackHdPrice = it.trackHdPrice,
            trackHdRentalPrice = it.trackHdRentalPrice,
            releaseDate = it.releaseDate,
            collectionExplicitness = it.collectionExplicitness,
            trackExplicitness = it.trackExplicitness,
            trackCount = it.trackCount,
            trackNumber = it.trackNumber,
            trackTimeMillis = it.trackTimeMillis,
            country = it.country,
            currency = it.currency,
            primaryGenreName = it.primaryGenreName,
            contentAdvisoryRating = it.contentAdvisoryRating,
            shortDescription = it.shortDescription,
            longDescription = it.longDescription
        )
    }
}

fun List<TrackEntity>.entityModelToTrackItem(): List<TrackItem> {
    return this.map {
        TrackItem(
            kind = it.kind,
            trackId = it.trackId,
            artistName = it.artistName,
            collectionName = it.collectionName,
            trackName = it.trackName,
            artworkUrl = it.artworkUrl,
            collectionPrice = it.collectionPrice,
            trackPrice = it.trackPrice,
            trackRentalPrice = it.trackRentalPrice,
            collectionHdPrice = it.collectionHdPrice,
            trackHdPrice = it.trackHdPrice,
            trackHdRentalPrice = it.trackHdRentalPrice,
            releaseDate = it.releaseDate,
            collectionExplicitness = it.collectionExplicitness,
            trackExplicitness = it.trackExplicitness,
            trackCount = it.trackCount,
            trackNumber = it.trackNumber,
            trackTimeMillis = it.trackTimeMillis,
            country = it.country,
            currency = it.currency,
            primaryGenreName = it.primaryGenreName,
            contentAdvisoryRating = it.contentAdvisoryRating,
            shortDescription = it.shortDescription,
            longDescription = it.longDescription
        )
    }
}

fun List<TrackItem>.trackItemToEntityModel(): List<TrackEntity> {
    return this.map {
        TrackEntity(
            id = 0,
            kind = it.kind,
            trackId = it.trackId,
            artistName = it.artistName,
            collectionName = it.collectionName,
            trackName = it.trackName,
            artworkUrl = it.artworkUrl,
            collectionPrice = it.collectionPrice,
            trackPrice = it.trackPrice,
            trackRentalPrice = it.trackRentalPrice,
            collectionHdPrice = it.collectionHdPrice,
            trackHdPrice = it.trackHdPrice,
            trackHdRentalPrice = it.trackHdRentalPrice,
            releaseDate = it.releaseDate,
            collectionExplicitness = it.collectionExplicitness,
            trackExplicitness = it.trackExplicitness,
            trackCount = it.trackCount,
            trackNumber = it.trackNumber,
            trackTimeMillis = it.trackTimeMillis,
            country = it.country,
            currency = it.currency,
            primaryGenreName = it.primaryGenreName,
            contentAdvisoryRating = it.contentAdvisoryRating,
            shortDescription = it.shortDescription,
            longDescription = it.longDescription
        )
    }
}
