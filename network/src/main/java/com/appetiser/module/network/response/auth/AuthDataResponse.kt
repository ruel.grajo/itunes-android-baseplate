package com.appetiser.module.network.response.auth

import com.appetiser.module.network.response.BaseResponse

data class AuthDataResponse(val data: UserDataResponse) : BaseResponse()
