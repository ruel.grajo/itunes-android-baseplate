package com.appetiser.module.network.response.country_codes

import com.appetiser.module.network.models.RawCountryCode

open class CountryCodeDataResponse(
    val countries: List<RawCountryCode>
)
