package com.appetiser.module.network.models

import com.appetiser.module.network.models.base.RawBaseUser
import com.google.gson.annotations.SerializedName

class RawUserSession(
    @field:SerializedName("phone_number")
    val phoneNumber: String? = "",
    @field:SerializedName("email_verified")
    val emailVerified: Boolean? = false,
    @field:SerializedName("phone_number_verified")
    val phoneNumberVerified: Boolean? = false,
    val verified: Boolean? = false,
    val email: String = ""
) : RawBaseUser()
