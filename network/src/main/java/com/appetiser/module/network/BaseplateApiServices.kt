package com.appetiser.module.network

import com.appetiser.module.network.response.SearchTrackResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface BaseplateApiServices {

    @GET("search")
    fun searchTrack(@Query("term") term: String, @Query("country") country: String, @Query("media") media: String): Single<SearchTrackResponse>
}
