package com.appetiser.module.network.response.auth.emailcheck

import com.appetiser.module.network.response.BaseResponse

data class EmailCheckDataResponse(val data: EmailExistDataResponse) : BaseResponse()
