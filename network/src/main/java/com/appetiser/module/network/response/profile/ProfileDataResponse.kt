package com.appetiser.module.network.response.profile

import com.appetiser.module.network.models.RawUserSession
import com.appetiser.module.network.response.BaseResponse

data class ProfileDataResponse(val data: RawUserSession) : BaseResponse()
