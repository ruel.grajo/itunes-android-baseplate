package com.appetiser.module.network.response.auth

import com.appetiser.module.network.models.RawUserSession
import com.google.gson.annotations.SerializedName

open class UserDataResponse(
    val user: RawUserSession,
    @field:SerializedName("access_token")
    val accessToken: String = "",
    @field:SerializedName("token_type")
    val tokenType: String? = "",
    @field:SerializedName("expires_in")
    val expiresIn: Long? = 0
)
