package com.appetiser.module.network.response.country_codes

import com.appetiser.module.network.response.BaseResponse

data class CountryCodeResponse(val data: CountryCodeDataResponse) : BaseResponse()
