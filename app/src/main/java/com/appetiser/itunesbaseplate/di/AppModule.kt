package com.appetiser.itunesbaseplate.di

import android.content.Context
import com.appetiser.itunesbaseplate.ItunesbaseplateApplication
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class AppModule {

    @Singleton
    @Binds
    abstract fun providesApplicationContext(app: ItunesbaseplateApplication): Context
}
