package com.appetiser.itunesbaseplate.di

import android.app.Application
import com.appetiser.itunesbaseplate.ItunesbaseplateApplication
import com.appetiser.itunesbaseplate.di.builders.ActivityBuilder
import com.appetiser.module.StorageModule
import com.appetiser.module.network.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AndroidInjectionModule::class,
            StorageModule::class,
            NetworkModule::class,
            ActivityBuilder::class,
            SchedulerModule::class,
            ViewModelFactoryModule::class,
            ViewModelModule::class,
            AppModule::class
        ]
)
interface ApplicationComponent : AndroidInjector<ItunesbaseplateApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun storageModule(storageModule: StorageModule): Builder
        fun networkModule(networkModule: NetworkModule): Builder
        fun schedulerModule(schedulerModule: SchedulerModule): Builder
        fun build(): ApplicationComponent
    }
}
