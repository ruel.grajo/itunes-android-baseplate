package com.appetiser.itunesbaseplate.di

import com.appetiser.itunesbaseplate.utils.schedulers.BaseSchedulerProvider
import com.appetiser.itunesbaseplate.utils.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SchedulerModule {

    @Provides
    @Singleton
    fun providesSchedulerSource(): BaseSchedulerProvider =
            SchedulerProvider.getInstance()
}
