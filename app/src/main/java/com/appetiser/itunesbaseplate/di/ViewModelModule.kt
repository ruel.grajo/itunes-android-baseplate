package com.appetiser.itunesbaseplate.di

import androidx.lifecycle.ViewModel
import com.appetiser.itunesbaseplate.di.scopes.ViewModelKey
import com.appetiser.itunesbaseplate.features.auth.splash.SplashViewModel
import com.appetiser.itunesbaseplate.features.track.viewModel.TrackListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TrackListViewModel::class)
    abstract fun bindTrackListViewModel(viewModel: TrackListViewModel): ViewModel
}
