package com.appetiser.itunesbaseplate.di.builders

import com.appetiser.itunesbaseplate.di.scopes.ActivityScope
import com.appetiser.itunesbaseplate.di.scopes.FragmentScope
import com.appetiser.itunesbaseplate.features.auth.TrackRepositoryModule
import com.appetiser.itunesbaseplate.features.auth.splash.SplashScreenActivity
import com.appetiser.itunesbaseplate.features.main.MainActivity
import com.appetiser.itunesbaseplate.features.main.MainRepositoryModule
import com.appetiser.itunesbaseplate.features.track.fragment.TrackListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [(MainRepositoryModule::class)])
    abstract fun contributeMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(TrackRepositoryModule::class)])
    abstract fun contributeSplashScreenActivity(): SplashScreenActivity

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeTrackListFragment(): TrackListFragment
}
