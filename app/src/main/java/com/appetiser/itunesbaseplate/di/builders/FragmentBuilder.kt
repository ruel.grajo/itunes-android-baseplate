package com.appetiser.itunesbaseplate.di.builders

import com.appetiser.itunesbaseplate.di.scopes.FragmentScope
import com.appetiser.itunesbaseplate.features.track.fragment.TrackListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeTrackListFragment(): TrackListFragment
}
