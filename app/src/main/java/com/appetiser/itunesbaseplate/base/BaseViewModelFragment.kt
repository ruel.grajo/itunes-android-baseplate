package com.appetiser.itunesbaseplate.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import java.lang.reflect.ParameterizedType
import javax.inject.Inject

abstract class BaseViewModelFragment<T : ViewDataBinding, V : BaseViewModel> : DaggerFragment() {
    private lateinit var viewDataBinding: T

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    lateinit var viewModel: V

    protected abstract fun createLayout(): Int

    protected abstract fun initData()

    protected abstract fun initViews()

    protected abstract fun initViewModel(view: View, viewModel: V)

    val disposables: CompositeDisposable = CompositeDisposable()

    @Suppress("UNCHECKED_CAST")
    private fun getViewModelClass() = (javaClass.genericSuperclass as ParameterizedType)
        .actualTypeArguments[1] as Class<V>

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initData()
        initViews()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders
            .of(this, factory)
            .get(getViewModelClass())
        viewModel.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, createLayout(), container, false)
        viewDataBinding.lifecycleOwner = this
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel(view, viewModel)
    }

    override fun onDestroyView() {
        disposables.clear()
        super.onDestroyView()
    }
}
