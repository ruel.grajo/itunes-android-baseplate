package com.appetiser.itunesbaseplate

import com.appetiser.itunesbaseplate.di.DaggerApplicationComponent
import com.appetiser.itunesbaseplate.di.SchedulerModule
import com.appetiser.module.StorageModule
import com.appetiser.module.network.NetworkModule
import com.facebook.stetho.Stetho
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber

class ItunesbaseplateApplication : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerApplicationComponent
            .builder()
            .application(this)
            .storageModule(StorageModule())
            .networkModule(NetworkModule())
            .schedulerModule(SchedulerModule())
            .build()
    }
}
