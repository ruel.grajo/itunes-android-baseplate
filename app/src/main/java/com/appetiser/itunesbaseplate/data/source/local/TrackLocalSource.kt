package com.appetiser.itunesbaseplate.data.source.local

import android.content.SharedPreferences
import com.appetiser.itunesbaseplate.data.TrackSource
import com.appetiser.module.common.LAST_KEYWORD_SEARCHED
import com.appetiser.module.common.stringValue
import com.appetiser.module.data.mapper.entityModelToTrackItem
import com.appetiser.module.data.mapper.trackItemToEntityModel
import com.appetiser.module.data.poko.TrackItem
import com.appetiser.module.local.AppDatabase
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class TrackLocalSource @Inject
constructor(private val sharedPreferences: SharedPreferences, private val database: AppDatabase) : TrackSource {

    override fun getLastSearchTrack(): Single<List<TrackItem>> {
        return database.trackDao().getAllTracks()
            .map {
                it.entityModelToTrackItem()
            }
            .onErrorReturnItem(emptyList())
    }

    override fun saveSearchTrack(trackList: List<TrackItem>): Completable {
        return Completable.create {
            database.trackDao().deleteTracks()
            database.trackDao().saveTracks(trackList.trackItemToEntityModel())
            it.onComplete()
        }
    }

    fun saveKeyword(keyword: String) {
        sharedPreferences.stringValue(LAST_KEYWORD_SEARCHED, keyword)
    }

    fun getLastKnownKeyword() = sharedPreferences.stringValue(LAST_KEYWORD_SEARCHED)
}
