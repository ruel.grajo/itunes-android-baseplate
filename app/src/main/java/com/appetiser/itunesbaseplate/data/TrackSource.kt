package com.appetiser.itunesbaseplate.data

import com.appetiser.module.data.poko.TrackItem
import io.reactivex.Completable
import io.reactivex.Single

interface TrackSource {

    fun searchTrack(term: String, country: String, media: String): Single<List<TrackItem>> =
        Single.just(emptyList())

    fun getLastSearchTrack(): Single<List<TrackItem>> =
        Single.just(emptyList())

    fun saveSearchTrack(trackList: List<TrackItem>): Completable =
        Completable.complete()
}
