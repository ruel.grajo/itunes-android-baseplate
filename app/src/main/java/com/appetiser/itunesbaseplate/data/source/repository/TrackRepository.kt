package com.appetiser.itunesbaseplate.data.source.repository

import com.appetiser.itunesbaseplate.data.TrackSource
import com.appetiser.itunesbaseplate.data.source.local.TrackLocalSource
import com.appetiser.itunesbaseplate.data.source.remote.TrackRemoteSource
import com.appetiser.module.data.poko.TrackItem
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class TrackRepository @Inject
constructor(private val remote: TrackRemoteSource, private val local: TrackLocalSource) : TrackSource {

    override fun searchTrack(term: String, country: String, media: String): Single<List<TrackItem>> {
        return remote.searchTrack(term, country, media)
            .doOnSuccess { saveSearchTrack(it).subscribe() }
            .doAfterSuccess { local.saveKeyword(term) }
    }

    override fun getLastSearchTrack(): Single<List<TrackItem>> {
        return local.getLastSearchTrack()
    }

    override fun saveSearchTrack(trackList: List<TrackItem>): Completable {
        return local.saveSearchTrack(trackList)
    }

    fun getLastKnownKeyword() = local.getLastKnownKeyword()
}
