package com.appetiser.itunesbaseplate.data.source.remote

import com.appetiser.itunesbaseplate.data.TrackSource
import com.appetiser.module.data.mapper.toTrackItemList
import com.appetiser.module.data.poko.TrackItem
import com.appetiser.module.network.BaseplateApiServices
import io.reactivex.Single
import javax.inject.Inject

class TrackRemoteSource @Inject
constructor(private val baseplateApiServices: BaseplateApiServices) : BaseRemoteSource(),
    TrackSource {

    override fun searchTrack(term: String, country: String, media: String): Single<List<TrackItem>> {
        return baseplateApiServices.searchTrack(term, country, media)
            .map {
                it.results.toTrackItemList()
            }
    }
}
