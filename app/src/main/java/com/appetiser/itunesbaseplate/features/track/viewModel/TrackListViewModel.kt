package com.appetiser.itunesbaseplate.features.track.viewModel

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.itunesbaseplate.R
import com.appetiser.itunesbaseplate.base.BaseViewModel
import com.appetiser.itunesbaseplate.data.source.repository.TrackRepository
import com.appetiser.itunesbaseplate.features.track.state.TrackListState
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class TrackListViewModel @Inject constructor(
    private val repository: TrackRepository
) : BaseViewModel() {

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
    }

    private val _state: MutableLiveData<TrackListState> by lazy {
        MutableLiveData<TrackListState>()
    }

    val state: LiveData<TrackListState>
        get() {
            return _state
        }

    fun searchTrack(keyword: String) {
        _state.value = TrackListState.HideCachedDataLabel
        _state.value = TrackListState.ShowProgressLoading

        disposables.add(
            repository.searchTrack(keyword, "au", "movie")
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribeBy(
                    onSuccess = {
                        _state.value = TrackListState.HideProgressLoading
                        _state.value = TrackListState.HideErrorMessage
                        _state.value = TrackListState.SearchTrackSuccess(it)
                    },
                    onError = {
                        _state.value = TrackListState.HideProgressLoading
                        _state.value = TrackListState.ShowErrorMessage(R.string.generic_error)
                    }
                )
        )
    }

    fun refreshList() {
        _state.value = TrackListState.ShowProgressLoading

        disposables.add(
            repository.searchTrack(repository.getLastKnownKeyword(), "au", "movie")
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribeBy(
                    onSuccess = {
                        _state.value = TrackListState.HideProgressLoading
                        _state.value = TrackListState.HideErrorMessage
                        _state.value = TrackListState.SearchTrackSuccess(it)
                    },
                    onError = {
                        _state.value = TrackListState.HideProgressLoading
                        _state.value = TrackListState.ShowErrorMessage(R.string.generic_error)
                    }
                )
        )
    }

    fun getCachedData() {
        if (repository.getLastKnownKeyword().isEmpty() || _state.value != null) {
            return
        }
        _state.value = TrackListState.ShowProgressLoading
        disposables.add(
            repository.getLastSearchTrack()
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribeBy(
                    onSuccess = {
                        _state.value = TrackListState.ShowCachedDataLabel(Pair(R.string.lbl_cached, repository.getLastKnownKeyword()))
                        _state.value = TrackListState.HideProgressLoading
                        _state.value = TrackListState.HideErrorMessage
                        _state.value = TrackListState.SearchTrackSuccess(it)
                    },
                    onError = {
                        _state.value = TrackListState.HideProgressLoading
                        _state.value = TrackListState.ShowErrorMessage(R.string.generic_error)
                    }
                )
        )
    }

    fun handleQueryTextSubmit(query: String?): Boolean {
        query?.let {
            searchTrack(it)
        }
        return false
    }
}
