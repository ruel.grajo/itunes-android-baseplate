package com.appetiser.itunesbaseplate.features.main

import com.appetiser.itunesbaseplate.features.auth.TrackRepositoryModule
import dagger.Module

@Module(includes = [(TrackRepositoryModule::class)])
class MainRepositoryModule
