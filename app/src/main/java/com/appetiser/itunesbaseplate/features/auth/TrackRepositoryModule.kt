package com.appetiser.itunesbaseplate.features.auth

import android.content.SharedPreferences
import com.appetiser.itunesbaseplate.data.source.local.TrackLocalSource
import com.appetiser.itunesbaseplate.data.source.remote.TrackRemoteSource
import com.appetiser.itunesbaseplate.data.source.repository.TrackRepository
import com.appetiser.itunesbaseplate.di.scopes.ActivityScope
import com.appetiser.module.local.AppDatabase
import com.appetiser.module.network.BaseplateApiServices
import dagger.Module
import dagger.Provides

@Module
class TrackRepositoryModule {

    @ActivityScope
    @Provides
    fun providesAuthLocalSource(sharefPref: SharedPreferences, database: AppDatabase):
            TrackLocalSource = TrackLocalSource(sharefPref, database)

    @ActivityScope
    @Provides
    fun providesAuthRemoteSource(baseplateApiServices: BaseplateApiServices):
            TrackRemoteSource = TrackRemoteSource(baseplateApiServices)

    @ActivityScope
    @Provides
    fun providesAuthRepository(remote: TrackRemoteSource, local: TrackLocalSource): TrackRepository =
            TrackRepository(remote, local)
}
