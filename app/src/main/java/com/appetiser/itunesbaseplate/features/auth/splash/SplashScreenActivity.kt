package com.appetiser.itunesbaseplate.features.auth.splash

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.appetiser.itunesbaseplate.R
import com.appetiser.itunesbaseplate.base.BaseViewModelActivity
import com.appetiser.itunesbaseplate.features.main.MainActivity

class SplashScreenActivity : BaseViewModelActivity<ViewDataBinding, SplashViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            context.startActivity(Intent(context, SplashScreenActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViewModels()
    }

    override fun getLayoutId(): Int = R.layout.activity_splash

    private fun setupViewModels() {
        viewModel.state.observe(this, Observer { state ->
            when (state) {
                is SplashState.HideProgressLoading -> {
                    MainActivity.openActivity(this)
                    finish()
                }
            }
        })

        viewModel.doPostDelay()
    }
}
