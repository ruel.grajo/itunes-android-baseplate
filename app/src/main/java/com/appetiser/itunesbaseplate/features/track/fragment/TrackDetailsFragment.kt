package com.appetiser.itunesbaseplate.features.track.fragment

import android.os.Bundle
import android.view.View
import com.appetiser.itunesbaseplate.R
import com.appetiser.itunesbaseplate.base.BaseFragment
import com.appetiser.itunesbaseplate.databinding.TrackDetailsFragmentBinding
import com.appetiser.itunesbaseplate.ext.loadAvatarUrl
import com.appetiser.module.common.formatStringDate
import com.appetiser.module.common.formatToDisplayTime
import com.appetiser.module.common.supportHTMLTags
import com.appetiser.module.data.poko.TrackItem
import kotlinx.android.synthetic.main.track_details_fragment.*

class TrackDetailsFragment : BaseFragment<TrackDetailsFragmentBinding>() {

    private var selectedTrackItem: TrackItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            selectedTrackItem = it.getSerializable(ARG_PARAM_ITEM) as TrackItem
        }
    }

    override fun getLayoutId() = R.layout.track_details_fragment

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        updateContent(selectedTrackItem)
    }

    fun updateContent(trackItem: TrackItem?) {
        selectedTrackItem = trackItem
        selectedTrackItem?.run {
            trackDetailsContainer.visibility = View.VISIBLE
            imageLoader.loadAvatarUrl(this.artworkUrl)

            trackNameTextView.text = this.trackName
            trackArtistTextView.text = getString(R.string.lbl_by, this.artistName)

            this.releaseDate?.let {
                releaseDateTextView.text = getString(R.string.lbl_release_date, it.formatStringDate()).supportHTMLTags()
            }
            this.trackTimeMillis?.let {
                trackLengthTextView.text = getString(R.string.lbl_track_length, it.formatToDisplayTime()).supportHTMLTags()
            }

            genreTextView.text = getString(R.string.lbl_track_genre, this.primaryGenreName).supportHTMLTags()

            priceTextView.text = getString(R.string.lbl_track_price, this.trackPrice.toString(), this.currency).supportHTMLTags()

            trackDescriptionTextView.text = this.longDescription
        }
    }

    companion object {
        private const val ARG_PARAM_ITEM = "argParamTrackItem"

        fun newInstance(trackItem: TrackItem): TrackDetailsFragment {
            val fragment = TrackDetailsFragment()
            val args = Bundle()
            args.putSerializable(ARG_PARAM_ITEM, trackItem)
            fragment.arguments = args
            return fragment
        }
    }
}
