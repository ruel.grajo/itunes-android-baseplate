package com.appetiser.itunesbaseplate.features.auth.splash

sealed class SplashState {

    object ShowProgressLoading : SplashState()

    object HideProgressLoading : SplashState()
}
