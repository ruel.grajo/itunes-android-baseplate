package com.appetiser.itunesbaseplate.features.track.fragment

import android.content.Context
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.appetiser.itunesbaseplate.R
import com.appetiser.itunesbaseplate.base.BaseViewModelFragment
import com.appetiser.itunesbaseplate.databinding.TrackListFragmentBinding
import com.appetiser.itunesbaseplate.features.track.adapter.TrackListAdapter
import com.appetiser.itunesbaseplate.features.track.state.TrackListState
import com.appetiser.itunesbaseplate.features.track.viewModel.TrackListViewModel
import com.appetiser.module.common.MarginItemDecoration
import com.appetiser.module.common.setLabelWithVisibility
import com.appetiser.module.data.poko.TrackItem
import kotlinx.android.synthetic.main.track_list_fragment.*

class TrackListFragment : BaseViewModelFragment<TrackListFragmentBinding, TrackListViewModel>(),
    SearchView.OnQueryTextListener {

    private var mListener: OnTrackListFragmentListener? = null

    private lateinit var trackListAdapter: TrackListAdapter

    override fun createLayout() = R.layout.track_list_fragment

    override fun initViewModel(view: View, viewModel: TrackListViewModel) {
        viewModel.state.observe(this, Observer {
            when (it) {
                is TrackListState.SearchTrackSuccess -> {
                    trackListAdapter.updateDataSet(it.listTracks)
                }

                is TrackListState.ShowCachedDataLabel -> {
                    cachedLabelTextView.setLabelWithVisibility(getString(it.label.first, it.label.second))
                }

                is TrackListState.HideCachedDataLabel -> {
                    cachedLabelTextView.setLabelWithVisibility(null)
                }

                is TrackListState.ShowProgressLoading -> {
                    trackSwipeToRefresh.isRefreshing = true
                }

                is TrackListState.HideProgressLoading -> {
                    trackSwipeToRefresh.isRefreshing = false
                }

                is TrackListState.ShowErrorMessage -> {
                    errorMessageTextView.setLabelWithVisibility(getString(it.message))
                    trackListAdapter.updateDataSet(emptyList())
                }

                is TrackListState.HideErrorMessage -> {
                    errorMessageTextView.setLabelWithVisibility(null)
                }
            }
        })
    }

    override fun initData() {}

    override fun initViews() {
        trackListAdapter = TrackListAdapter(object : TrackListAdapter.TrackListAdapterListener {
            override fun onTrackSelect(trackItem: TrackItem) {
                mListener?.onTrackItemClicked(trackItem)
            }
        })
        with(trackRecyclerView) {
            layoutManager = GridLayoutManager(context, SPAN_COUNT)
            adapter = trackListAdapter
            addItemDecoration(MarginItemDecoration(resources.getDimension(R.dimen.space_unit_2).toInt(), SPAN_COUNT))
        }

        trackSearchView.setOnQueryTextListener(this)

        trackSwipeToRefresh.setOnRefreshListener {
            viewModel.refreshList()
        }
        viewModel.getCachedData()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnTrackListFragmentListener) {
            mListener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onQueryTextSubmit(query: String?) = viewModel.handleQueryTextSubmit(query)

    override fun onQueryTextChange(newText: String?) = false

    companion object {
        const val SPAN_COUNT = 2
        fun newInstance() = TrackListFragment()
    }

    /**
     * Interface to handle callbacks
     * */
    interface OnTrackListFragmentListener {
        /**
         * Function to handle list item clicked callback to class that implement it.
         *
         * @param trackItem the selected track item
         * */
        fun onTrackItemClicked(trackItem: TrackItem)
    }
}
