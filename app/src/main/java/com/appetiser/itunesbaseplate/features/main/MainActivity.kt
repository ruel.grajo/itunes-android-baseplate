package com.appetiser.itunesbaseplate.features.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.FrameLayout
import com.appetiser.itunesbaseplate.R
import com.appetiser.itunesbaseplate.base.BaseActivity
import com.appetiser.itunesbaseplate.databinding.ActivityMainBinding
import com.appetiser.itunesbaseplate.features.track.fragment.TrackDetailsFragment
import com.appetiser.itunesbaseplate.features.track.fragment.TrackListFragment
import com.appetiser.module.common.navigate
import com.appetiser.module.data.poko.TrackItem
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<ActivityMainBinding>(), TrackListFragment.OnTrackListFragmentListener {

    companion object {
        fun openActivity(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(intent)
        }
    }

    private var backEnable = false

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showToolbar()
        setToolbarBackgroundColor(R.color.dark_gray)
        setToolbarTitle(R.string.title_search_track)
        if (layout_container is FrameLayout) {
            TrackListFragment.newInstance().navigate(supportFragmentManager, R.id.layout_container, false)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setToolbarTitle(R.string.title_search_track)
        disableToolbarBackButton()
    }

    override fun canBack(): Boolean {
        return backEnable
    }

    override fun onTrackItemClicked(trackItem: TrackItem) {
        val trackDetailsFragment: TrackDetailsFragment? = supportFragmentManager.findFragmentById(R.id.layout_container_details) as TrackDetailsFragment?

        if (trackDetailsFragment == null) {
            enableToolbarHomeIndicator()
            trackItem.trackName?.let {
                setToolbarTitle(it)
            }
            backEnable = true
            TrackDetailsFragment.newInstance(trackItem).navigate(supportFragmentManager, R.id.layout_container, true)
        } else {
            trackDetailsFragment.updateContent(trackItem)
        }
    }
}
