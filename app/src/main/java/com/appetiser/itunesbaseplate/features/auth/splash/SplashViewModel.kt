package com.appetiser.itunesbaseplate.features.auth.splash

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.itunesbaseplate.base.BaseViewModel
import com.appetiser.itunesbaseplate.data.source.repository.TrackRepository
import io.reactivex.Completable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SplashViewModel @Inject constructor(private val repository: TrackRepository) : BaseViewModel() {

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
    }

    private val _state: MutableLiveData<SplashState> by lazy {
        MutableLiveData<SplashState>()
    }

    val state: LiveData<SplashState>
        get() {
            return _state
        }

    fun doPostDelay() {
        _state.value = SplashState.ShowProgressLoading
        disposables.add(Completable.complete()
            .delay(500, TimeUnit.MILLISECONDS)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doFinally {
                _state.value = SplashState.HideProgressLoading
            }
            .subscribe()
        )
    }
}
