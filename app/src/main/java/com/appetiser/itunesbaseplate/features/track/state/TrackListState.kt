package com.appetiser.itunesbaseplate.features.track.state

import com.appetiser.module.data.poko.TrackItem

sealed class TrackListState {

    data class SearchTrackSuccess(val listTracks: List<TrackItem>) : TrackListState()

    object ShowProgressLoading : TrackListState()

    object HideProgressLoading : TrackListState()

    data class ShowCachedDataLabel(val label: Pair<Int, String>) : TrackListState()

    object HideCachedDataLabel : TrackListState()

    object HideErrorMessage : TrackListState()

    data class ShowErrorMessage(val message: Int) : TrackListState()
}
